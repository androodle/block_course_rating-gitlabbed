<?php
/**
 * Course Rating
 *
 * @package    block
 * @copyright  Eamon Delaney
 */

require_once( '../../config.php' );
require_once('lib.php');

global $COURSE, $PAGE, $DB, $USER;

$courseid = required_param( 'courseid', PARAM_INT );
$rating = required_param( 'rating', PARAM_INT );
//  Load the course.
$context = get_context_instance(CONTEXT_COURSE, $courseid);

//  Require user to be logged in to view this page.
if ((!isloggedin() || isguestuser())) {
    exit();
}

//Is Rating within range?
if($rating > 5 || $rating < 0)
{
	exit;
}

//And finally they need the ability to rate
require_capability('block/course_rating:rate', $context);

$block = block_instance('course_rating');

$res = array();

$conds = array("courseid"=>$courseid, "userid"=>$USER->id);

if($DB->record_exists("block_course_rating", $conds))
{
	$rate_record = $DB->get_record("block_course_rating", $conds);
	$rate_record->rating = $rating;
	$DB->update_record("block_course_rating", $rate_record);
	$res["message"] = "Rating Updated";
}
else
{
	$rate_record = new stdClass;
	$rate_record->courseid = $courseid;
	$rate_record->rating = $rating;
	$rate_record->userid = $USER->id;
	$DB->insert_record("block_course_rating", $rate_record);
	$res["message"] = "Rating Added";
}

$res["myrating"] = $rating;
$res["avgrating"] = get_average_rating($courseid);
$res["countrating"] = get_count_rating($courseid);

echo json_encode($res);
